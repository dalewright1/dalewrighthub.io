# Contact

- [Email](mailto:dale@dalewright.com)
- [Twitter @dalecraigwright](https://www.twitter.com/dalecraigwright)
- [LinkedIn @dalecraigwright](https://linkedin.com/in/dalecraigwright)
- Video chat (upon request)
