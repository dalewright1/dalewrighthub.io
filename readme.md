{% seo %}

## [Professional History](#professionalhistory)

### Lead Clinical Strategist
**Cerner, Australia**
September 2017 - Present

### Senior Clinical Strategist
**Cerner, Australia**
March 2016 - September 2017

Workflow and change management expert. Accountable for helping clients achieve measurable value from Cerner's best-practice workflows and technology solutions.

### Clinical Strategist
**Cerner, United Kingdom & Australia**
April 2012 - March 2016

Workflow specialist. Responsible for helping clients transform their clinical and business processes, by leveraging Cerner’s best-practice workflows and technology solutions.

### Strategic Business Analyst
**Cerner, United Kingdom**
August 2011 - April 2012

Aligned to senior executive and their leadership team. Assisted with strategic business planning.

### Senior Consultant
**Cerner, United Kingdom**
April 2010 - August 2011

Solution expert. Accountable for successful implementation of Cerner's technology solutions. Area of focus included; acute medical, pharmacy, nursing, allied health and clerical.

###  Consultant
**Cerner, United Kingdom**
March 2008 - April 2010

Solution specialist. Responsible for technical configuration of Cerner's technology solutions. Area of focus included; acute medical, nursing and allied health.

### Service Management Intern
**IBM Global Services, United Kingdom**
June 2005 - June 2006

Assisted with the resolution of business-critical technology incidents, leveraging ITIL Service Management best practices.

### Senior Campsite Courier
**Canvas Holidays, Vendée, France**
May 2004 - November 2004

Customer service and operational management of an overseas family holiday resort.

### Lead Bartender
**Luminar Leisure, Nottingham, United Kingdom**
2004 - 2005

Customer service, operational management and financial accounting for multiple service areas within large entertainment venue.

### Campsite Courier
**Canvas Holidays, Vendée, France**
June 2003 - September 2003

Customer service and accommodation maintenance for an overseas family holiday resort.

### Bartender
**Luminar Leisure, Nottingham, United Kingdom**
2002 - 2004

Customer service at large entertainment venue.

## [Professional Projects & Accomplishments](#projectsalaccomplishments)

### St Stephen's Hospital, United Care Health
**Hervey Bay, Queensland, Australia**
2013 - 2015

>  First HIMSS Stage 6 Hospital in Australia. <sup>1

### St Georges University Hospitals NHS Foundation Trust
**London, United Kingdom**
2011 – 2013

> First trust in UK validated HIMMS Stage 6 by an on-site visit. <sup>2

### Oxford University Hospitals NHS Trust
**Oxford, United Kingdom**
2010 – 2011

> Reduced risk of errors arising from having multiple systems by consolidating information in one place and reporting. <sup>3

### Kingston Hospital NHS Foundation Trust
**Kingston upon Thames, United Kingdom**
2008 - 2009

> The hospital went live on the final day of November, the very latest that director general of informatics, Christine Connelly, said that Local Service Providers BT and CSC were given to make “significant process” with their strategic systems under the National Programme for IT. <sup>4

## [Professional Awards](#professionalawards)

### Rising Star
**Cerner, United Kingdom**
2010

Awarded for my performance during the early years of my Cerner career. Lead to successful completion of Cerner's accelerated career development programme.

## [Education History](#educationalhistory)

### Bachelor of Arts, Quality & Business Excellence
**Nottingham Business School**
2002 - 2007

### Double A-Level in Business Studies &  A-Level in History
**Alcester Grammar School**
2000 - 2002

## [Personal Interests](#personalinterests)

- Emerging technologies
- Growing companies
- Mentoring and coaching

## [Further Information](#furtherinformation)

### Location

- Melbourne, Australia

### Language

- English

### Legal Status

- British citizen
- Resident of United Kingdom and Australia

### References

- References available upon request
- [Review my endorsements on LinkedIn](https://www.linkedin.com/in/dalecraigwright/)

## [Contact Information](#contactinformation)

### Direct

- [Email](mailto:dale@dalewright.com)
- Face-to-face, telephone or video chat ([Request contact information](mailto:dale@dalewright.com))

### Social Media

- [Twitter](https://www.twitter.com/dalecraigwright)
- [LinkedIn](https://linkedin.com/in/dalecraigwright)

###### Footnotes

<sup>1</sup> [Press release from HIMMS Analytics Asia](http://www.himssanalyticsasia.org/about/pressRoom-pressrelease19.asp).
<sup>2</sup> [Press release from St Georges NHS Foundation Trust](https://www.stgeorges.nhs.uk/newsitem/st-georges-receives-national-accreditation-himss-stage-6/).
<sup>3</sup> [Case study by Oxford University Hospitals NHS Foundation Trust](http://www.ouh.nhs.uk/patient-guide/documents/epr-case-study.pdf).
<sup>4</sup> [Industry news article](https://www.digitalhealth.net/2009/12/kingston-hits-go-live-date-with-cerner/).
